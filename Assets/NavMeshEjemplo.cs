﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshEjemplo : MonoBehaviour {

    public Camera camera;

    NavMeshAgent agente;
	// Use this for initialization
	void Start () {
        agente = GetComponent<NavMeshAgent>();
        agente.destination = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0)) {

            Ray rayito = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(rayito, out hit)) {
                agente.destination = hit.point;
            }
        }	
	}
}
