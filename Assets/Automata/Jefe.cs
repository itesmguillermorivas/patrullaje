﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jefe : MonoBehaviour {


    public Transform enemigo;
    public float limiteDeDistancia;

    // nodo inicial / actual
    private ANodo actual;
    private MonoBehaviour comportamiento;
    private Simbolo pasear, jugar, abrirBolsaDeComida;

    // Use this for initialization
    void Start()
    {
        // agregar comportamiento dinámicamente
        // gameObject.AddComponent<PoopBehaviour>();
        // gameObject.AddComponent(typeof(PoopBehaviour));

        // ESTADOS
        ANodo dormido = new ANodo("dormido", typeof(DormidoBehaviour));
        ANodo juegando = new ANodo("juegando", typeof(JuegandoBehaviour));
        ANodo poop = new ANodo("poopeando", typeof(PoopBehaviour)); // a petición de Mayra

        // LENGUAJE
        pasear = new Simbolo("pasear");
        jugar = new Simbolo("jugar");
        abrirBolsaDeComida = new Simbolo("comida");

        // FUNCIÓN DE TRANSICIÓN
        dormido.AddTransicion(pasear, juegando);
        dormido.AddTransicion(jugar, juegando);
        dormido.AddTransicion(abrirBolsaDeComida, poop);

        juegando.AddTransicion(pasear, dormido);
        juegando.AddTransicion(jugar, poop);
        juegando.AddTransicion(abrirBolsaDeComida, juegando);

        poop.AddTransicion(pasear, poop);
        poop.AddTransicion(jugar, dormido);
        poop.AddTransicion(abrirBolsaDeComida, juegando);

        // ESTADO INICIAL
        actual = dormido;
        comportamiento = gameObject.AddComponent(actual.Comportamiento) as MonoBehaviour;
        StartCoroutine(VerificarDistancia());
    }
	
	// Update is called once per frame
	void Update () {

        print(actual.Nombre);

        if (Input.GetKeyUp(KeyCode.P))
        {
            CambiarEstado(pasear);
        }
        else if (Input.GetKeyUp(KeyCode.J))
        {
            CambiarEstado(jugar);
        }
        else if (Input.GetKeyUp(KeyCode.A)) {
            CambiarEstado(abrirBolsaDeComida);
        }
	}

    void CambiarEstado(Simbolo simbolo) {

        ANodo temp = actual.AplicarTransicion(simbolo);

        if (actual != temp) {

            actual = temp;
            // deshacernos del behaviour actual
            Destroy(comportamiento);

            // reasignar referencia
            comportamiento = gameObject.AddComponent(actual.Comportamiento) as MonoBehaviour;
        }
    }

    IEnumerator VerificarDistancia() {

        while (true) {

            float distancia = Vector3.Distance(
                transform.position, 
                enemigo.position);

            if (distancia < limiteDeDistancia) {
                CambiarEstado(jugar);
            }

            yield return new WaitForSeconds(0.5f);
        }
    }
}
