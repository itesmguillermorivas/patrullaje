﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jugador : MonoBehaviour {

    public Camera camera;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonUp(0)) {

            // Lanzar rayito

            // obtener rayito
            Ray rayito = camera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(rayito, out hit))
            {
                print("SI PEGÓ " + hit.point);
                print("EL ENEMIGO: " + hit.transform.name);
            }
            else {
                print("NO PEGÓ");
            }
        }
	}

    void OnMouseUp() {
        print("MOUSE UP!");
    }
}
