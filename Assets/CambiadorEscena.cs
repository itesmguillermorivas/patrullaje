﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CambiadorEscena : MonoBehaviour {

    public void CambiarEscena() {
        SceneManager.LoadScene("SampleScene");
    }
}
