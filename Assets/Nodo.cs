﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nodo : MonoBehaviour {

    // propiedades
    // C#

    public float F {
        get { return g + h; }
    }

    public float g, h;
    public Nodo[] vecinos;
    public List<Nodo> historial;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    // sólo funciona en editor
    void OnDrawGizmos() {

        // lógica para dibujo de gizmos
        // gizmos - información gráfica de debug para desarrollo
        Gizmos.color = Color.green;

        Gizmos.DrawSphere(transform.position, 1);
        //Gizmos.DrawWireSphere(transform.position, 1);

        Gizmos.color = Color.blue;
        for (int i = 0; i < vecinos.Length; i++) {
            Gizmos.DrawLine(
                transform.position,
                vecinos[i].transform.position
                );
        }
    }

    void OnDrawGizmosSelected() {

        Gizmos.color = new Color(232, 158, 79);
        Gizmos.DrawSphere(transform.position, 1);
    }
}
