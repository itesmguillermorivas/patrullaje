﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emisor : MonoBehaviour {

    private AudioSource player;
    public AudioClip audio1;
    public AudioClip audio2;
    public AudioClip audio3;

	// Use this for initialization
	void Start () {
        player = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.A)) {
            player.clip = audio1;
            player.Play();
        }
        if (Input.GetKeyUp(KeyCode.B)) {
            player.clip = audio2;
            player.Play();
        }
        if (Input.GetKeyUp(KeyCode.C)) {
            player.loop = true;
            player.clip = audio3;
            player.Play();
        }

	}
}
