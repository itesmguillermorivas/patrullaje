﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding {

    public static List<Nodo> Breadthwise(Nodo inicio, Nodo fin) {

        Queue<Nodo> fila = new Queue<Nodo>();
        List<Nodo> visitado = new List<Nodo>();

        inicio.historial = new List<Nodo>();
        fila.Enqueue(inicio);
        visitado.Add(inicio);


        while (fila.Count > 0) {
            Nodo actual = fila.Dequeue();
            if (actual == fin) {

                // llegamos al resultado
                List<Nodo> resultado = actual.historial;
                resultado.Add(actual);
                return resultado;
            } else {

                for (int i = 0; i < actual.vecinos.Length; i++) {

                    Nodo vecinoActual = actual.vecinos[i];

                    if (!visitado.Contains(vecinoActual)) {
                        visitado.Add(vecinoActual);
                        fila.Enqueue(vecinoActual);

                        // agregar historial
                        vecinoActual.historial = new List<Nodo>(actual.historial);
                        vecinoActual.historial.Add(actual);

                    }
                }
            }
        }

        return null;
    }

    public static List<Nodo> Depthwise(Nodo inicio, Nodo fin)
    {

        Stack<Nodo> pila = new Stack<Nodo>();
        List<Nodo> visitado = new List<Nodo>();

        inicio.historial = new List<Nodo>();
        pila.Push(inicio);
        visitado.Add(inicio);


        while (pila.Count > 0)
        {
            Nodo actual = pila.Pop();
            if (actual == fin)
            {

                // llegamos al resultado
                List<Nodo> resultado = actual.historial;
                resultado.Add(actual);
                return resultado;
            }
            else
            {

                for (int i = 0; i < actual.vecinos.Length; i++)
                {

                    Nodo vecinoActual = actual.vecinos[i];

                    if (!visitado.Contains(vecinoActual))
                    {
                        visitado.Add(vecinoActual);
                        pila.Push(vecinoActual);

                        // agregar historial
                        vecinoActual.historial = new List<Nodo>(actual.historial);
                        vecinoActual.historial.Add(actual);

                    }
                }
            }
        }

        return null;
    }

    public static List<Nodo> AEstrella(Nodo inicio, Nodo fin) {

        List<Nodo> trabajo = new List<Nodo>();
        List<Nodo> visitados = new List<Nodo>();

        inicio.historial = new List<Nodo>();
        inicio.g = 0;
        inicio.h = Vector3.Distance(
            inicio.transform.position,
            fin.transform.position
            );

        trabajo.Add(inicio);
        visitados.Add(inicio);

        while (trabajo.Count > 0) {

            // buscar el siguiente
            Nodo actual = trabajo[0];

            for (int i = 1; i < trabajo.Count; i++) {
                if (trabajo[i].F < actual.F) {
                    actual = trabajo[i];
                }
            }

            trabajo.Remove(actual);

            if (actual == fin) {

                List<Nodo> resultado = actual.historial;
                resultado.Add(actual);
                return resultado;
            } else {

                for (int i = 0; i < actual.vecinos.Length; i++) {

                    Nodo vecinoActual = actual.vecinos[i];

                    if (vecinoActual == fin) {
                        List<Nodo> resultado = vecinoActual.historial;
                        resultado.Add(vecinoActual);
                        return resultado;
                    }

                    if (!visitados.Contains(vecinoActual)) {

                        vecinoActual.historial = new List<Nodo>(actual.historial);
                        vecinoActual.historial.Add(actual);

                        vecinoActual.g = actual.g +
                            Vector3.Distance(
                                actual.transform.position,
                                vecinoActual.transform.position
                                );

                        vecinoActual.h = Vector3.Distance(
                            actual.transform.position,
                            fin.transform.position
                            );

                        trabajo.Add(vecinoActual);
                        visitados.Add(vecinoActual);
                    }
                }
            }
        }

        return null;
    }
}
