﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PFTest : MonoBehaviour {

    public Nodo inicio, fin;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.B)) {
            List<Nodo> resultado = Pathfinding.Breadthwise(inicio, fin);
            for (int i = 0; i < resultado.Count; i++) {

                print(resultado[i].transform.name);
            }
        }
	}
}
